#########Instalao 
python 3.7 (Baixar no site, j vem com PIP e Virtual Env, porm  necessrio atualizar)
python -m pip install --upgrade pip

criar virtual venv: python -m venv venv
ativar virutal venv:  .\venv\Scripts\activate.bat (executar como administrador)
instalar django com virtual env ativado: pip install django
ou usar um arquivo requeriments.txt com as dependencias e usar: pip install -r requeriments.txt

#########Comandos e configurao do projeto
criar projeto (com virtual env ativado): django-admin startproject proj
configurar settings.py: nome do banco, LANGUAGE_CODE = 'pt-br' TIME_ZONE = 'America/Sao_Paulo'

instalar banco de dados: python manage.py migrate
startar o servidor: python manage.py runserver

criar o app: python manage.py startapp clientes
apos isso adicionar o app no settings.py na array  INSTALLED_APPS  e criar o arquivo urls.py para gerenciar as urls

criando banco de dados a partir das models (Toda vez q alterar ou adicionar uma model)
python manage.py makemigrations aps isso rodar python manage.py migrate

#########Administrao do projeto
localhost:port/admin 
usuario e senha a baixo  
criar um superusuario do banco: python manage.py createsuperuser
registrar um model no admin: ir no arquivo admin.py, importar o model(from .models import Model) e regitrar na funcao: admin.site.register(Model)



#########Mais
https://www.youtube.com/watch?v=-YlK0A8Goj8&t=1003s

doc django 2.1
https://docs.djangoproject.com/en/2.1/ref/templates/builtins/


heroku
https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Deployment



autenticao 
documentaao: https://docs.djangoproject.com/en/2.1/topics/auth/default/

https://www.youtube.com/watch?v=qNmDkCEDi7w

implementando=>https://www.youtube.com/watch?v=l8f-KFxw-xU


utilizando include (register)->https://www.youtube.com/watch?v=dBctY3-Z5hY
