from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, get_user

def register(request):
    if not request.user.is_authenticated:   
        context = {}
        if request.method == 'POST':
            form = UserCreationForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data['username']
                password = form.cleaned_data['password1']
                user = authenticate(username=username, password=password)
                login(request, user)
                return redirect('/')
            else:
                context['form'] = form
        else:
            context['form'] = UserCreationForm()    
        return render(request , 'registration/register.html' , context)
    else:
        return redirect('/')
